package net.dreamlu.weixin.config;

/**
 * 小程序消息解析
 *
 * @author L.cm
 */
public enum WxaMsgParser {
	JSON, XML
}
